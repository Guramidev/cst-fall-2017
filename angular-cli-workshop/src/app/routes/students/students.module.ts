import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentsRoutingModule } from './students-routing.module';
import { StudentsComponent } from './students.component';
import { ListComponent } from './list/list.component';
import { StudentComponent } from './student/student.component';

@NgModule({
  imports: [
    CommonModule,
    StudentsRoutingModule
  ],
  declarations: [StudentsComponent, ListComponent, StudentComponent]
})
export class StudentsModule { }
