import { Component, OnInit } from '@angular/core';
import {ListService} from "./list.service";
import {Student} from "../students.model";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-list',
  providers: [ListService],
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  students: Observable<Array<Student>> =
    this.listService.getStudents();

  constructor(
    private listService: ListService
  ) { }

  ngOnInit() {}
}
