import { Injectable } from '@angular/core';
import {ApiService} from "../../../shared/services/api/api.service";

@Injectable()
export class StudentService {
  constructor(
    private apiService: ApiService
  ) { }

  getStudent(id: number) {
    return this.apiService.get('students/' + id)
  }
}
