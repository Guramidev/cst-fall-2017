import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {StudentService} from "./student.service";
import {Observable} from "rxjs/Observable";
import {Student} from "../students.model";

@Component({
  selector: 'app-student',
  providers: [StudentService],
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit, OnDestroy {
  student$: Observable<Student>;
  student: Student;
  subscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private studentService: StudentService
  ) { }

  ngOnInit() {
    this.subscription = this.activatedRoute
      .params.subscribe((params) => {
        let id = +params.id;

        if (id) {
          this.student$ =
            this.studentService.getStudent(id);
        }
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
