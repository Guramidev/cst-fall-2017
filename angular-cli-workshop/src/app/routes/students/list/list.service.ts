import { Injectable } from '@angular/core';
import {ApiService} from "../../../shared/services/api/api.service";

@Injectable()
export class ListService {
  constructor(
    private apiService: ApiService
  ) { }

  getStudents() {
    return this.apiService.get('students')
  }
}
