import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MenuComponent } from './shared/components/menu/menu.component';
import {RouterModule} from "@angular/router";
import {ApiService} from "./shared/services/api/api.service";
import {HttpClientModule} from "@angular/common/http";

let routes = [{
  path: '',
  pathMatch: 'full',
  redirectTo: 'students'
}, {
  path: 'students',
  loadChildren: './routes/students/students.module#StudentsModule'
}];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
