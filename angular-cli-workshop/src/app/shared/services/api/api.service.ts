import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ApiService {
  private endpoint: string = 'http://localhost:3000';

  constructor(
    private httpClient: HttpClient
  ) { }

  get(url: string) {
    return <Observable<any>>this.httpClient
      .get(this.endpoint + '/' + url);
  }
}
